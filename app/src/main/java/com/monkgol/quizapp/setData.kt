package com.monkgol.quizapp

object setData {

    const val name:String="name"
    const val score:String="score"


    fun getQuestion():ArrayList<QuestionData>{
        var que:ArrayList<QuestionData> = arrayListOf()

        var question1 = QuestionData(
            1,
            "มหาลัยดังบางแสนคือ ?",

            "มหาวิทยาลัยเกษตรศาสตร์",
            "มหาวิทยาลัยมหิดล",
            "มหาวิทยาลัยธรรมมาศาสตร์",
            "มหาวิทยาลัยบูรพา",
            4
        )
        var question2 = QuestionData(
            2,
            "ยาสลบทำจากอะไร ?",

            "คลอโรฟอร์ม",
            "เคตามีน",
            "เฮโรอีน",
            "แอนไฮไดรด์",
            1
        )
        var question3 = QuestionData(
            3,
            "โพไซดอน เป็นเทพยเจ้าแห่งทะเลของใคร ?",

            "ชาวนา",
            "ชาวกรีก",
            "ชาวไทย",
            "ชาวจีน",
            2
        )
        var question4 = QuestionData(
            4,
            "โรคโลหิตจางเกิดจากการขาดสารอาหารอะไร ?",

            "แมกนีเซียม",
            "แคลเซียม",
            "เหล็ก",
            "ฟอสฟอรัส",
            3
        )

        var question5 = QuestionData(
            5,
            "ประเทศใดได้ชื่อจัดการกับขยะดีที่สุดในโลก ?",

            "พม่า",
            "ไทย",
            "จีน",
            "ญี่ปุ่น",
            4
        )
        var question6 = QuestionData(
            6,
            "งานวันกาชาดมีครั้งแรกในรัชสมัยใด ?",

            "รัชกาลที่ 9",
            "รัชกาลที่ 8",
            "รัชกาลที่ 6",
            "รัชกาลที่ 7",
            4
        )
        var question7 = QuestionData(
            7,
            "จังหวัดใดได้ชื่อมีพื้นที่ปลูกปาล์มน้ำมันมากที่สุด ?",

            "ภูเก็ต",
            "ชลบุรี",
            "กระบี่",
            "เชียงใหม่",
            3
        )
        var question8 = QuestionData(
            8,
            "ผักเก่าแก่ที่สุดที่มนุษย์เคยรู้จักคืออะไร ?",

            "กระท่อม",
            "ถั่ว",
            "กัญชา",
            "ผักบุ้ง",
            2
        )
        var question9 = QuestionData(
            9,
            "พระบาทสมเด็จพระปกเกล้าเจ้าอยู่หัวเสด็จสวรรคตที่ประเทศใด ?",

            "ประเทศอังกฤษ",
            "ประเทศไทย",
            "ประเทศจีน",
            "ประเทศญี่ปุ่น",
            1
        )
        var question10 = QuestionData(
            10,
            "ทุก ๆ กี่ปีดาวศุกร์จะโคจรเข้าใกล้โลกมากที่สุด ?",

            "7 ปี",
            "8 ปี",
            "9 ปี",
            "10 ปี",
            2
        )

        que.add(question1)
        que.add(question2)
        que.add(question3)
        que.add(question4)
        que.add(question5)
        que.add(question6)
        que.add(question7)
        que.add(question8)
        que.add(question9)
        que.add(question10)
        return que
    }
}